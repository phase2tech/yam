<?php


namespace Phase2\Package\Archiver;

use Composer\Package\Archiver\ArchiveManager;
use Composer\Downloader\DownloadManager;
use Composer\Package\PackageInterface;
use Composer\Package\RootPackage;
use Composer\Util\Filesystem;

class DrupalArchiveManager extends ArchiveManager {
    /**
     * Generate a distinct filename for a particular version of a package.
     *
     * @param PackageInterface $package The package to get a name for
     *
     * @return string A filename without an extension
     */
    public function getPackageFilename(PackageInterface $package)
    {
        $download_safe_name = preg_replace('#[^a-z0-9-_.]#i', '-', $package->getName());

        return $download_safe_name;
    }

    private function determineDrupalVersions(PackageInterface $package) {
        $versions = array();

        $version = $package->getPrettyVersion();
        $version_parts = explode('.', $version);
        
        $core_version = array_shift($version_parts);
        $core_version .= '.x';

        $major_version = array_shift($version_parts);

        $minor_version_string = array_shift($version_parts);
        $minor_version_parts = explode('-', $minor_version_string);
        $minor_version = array_shift($minor_version_parts);
        $version_extra = array_shift($minor_version_parts);

        $version_extra = empty($version_extra) ? NULL : $version_extra;

        $package->coreVersion = $core_version;
        $package->majorVersion = $major_version;
        if (is_numeric($minor_version)) {
            $package->minorVersion = $minor_version;
        }
        $package->versionExtra = $version_extra;

        $drupal_version = implode('-', array($core_version, implode('.', array($major_version, $minor_version_string))));
        $package->drupalVersion = $drupal_version;

        return $package;
    }

    /**
     * Create an archive of the specified package.
     *
     * @param  PackageInterface          $package   The package to archive
     * @param  string                    $format    The format of the archive (zip, tar, ...)
     * @param  string                    $targetDir The diretory where to build the archive
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @return string                    The path of the created archive
     */
    public function archive(PackageInterface $package, $format, $targetDir)
    {
        if (empty($format)) {
            throw new \InvalidArgumentException('Format must be specified');
        }

        // Search for the most appropriate archiver
        $usableArchiver = null;
        foreach ($this->archivers as $archiver) {
            if ($archiver->supports($format, $package->getSourceType())) {
                $usableArchiver = $archiver;
                break;
            }
        }

        // Checks the format/source type are supported before downloading the package
        if (null === $usableArchiver) {
            throw new \RuntimeException(sprintf('No archiver found to support %s format', $format));
        }

        $filesystem = new Filesystem();
        $packageName = $this->getPackageFilename($package);

        $package = $this->determineDrupalVersions($package);

        // Archive filename
        $filesystem->ensureDirectoryExists($targetDir);
        $target = realpath($targetDir).'/'.$package->drupalVersion.'/'.$packageName.'.'.$format;
        $filesystem->ensureDirectoryExists(dirname($target));

        if (!$this->overwriteFiles && file_exists($target)) {
            return $target;
        }

        if ($package instanceof RootPackage) {
            $sourcePath = realpath('.');
        } else {
            // Directory used to download the sources
            $sourcePath = sys_get_temp_dir().'/composer_archiver/'.$packageName;
            $filesystem->ensureDirectoryExists($sourcePath);

            // Download sources
            $this->downloadManager->download($package, $sourcePath);
        }

        // Add the version information to the .info file of the module.

        // Create the archive
        $archivePath = $usableArchiver->archive($sourcePath, $target, $format, $package->getArchiveExcludes());

        //cleanup temporary download
        if (!$package instanceof RootPackage) {
            $filesystem->removeDirectory($sourcePath);
        }

        return $archivePath;
    }
}