<?php

namespace Phase2;

use Composer\Factory;
use Composer\Config as Config;
use Composer\IO as IO;
use Composer\Downloader as Downloader;
use Composer\Package\Archiver as Archiver;
use Phase2\Package\Archiver\DrupalArchiveManager as DrupalArchiveManager;

class DrupalFactory extends Factory {
    /**
     * @param Config                     $config The configuration
     * @param Downloader\DownloadManager $dm     Manager use to download sources
     *
     * @return Archiver\ArchiveManager
     */
    public function createArchiveManager(Config $config, Downloader\DownloadManager $dm = null)
    {
        if (null === $dm) {
            $io = new IO\NullIO();
            $io->loadConfiguration($config);
            $dm = $this->createDownloadManager($io, $config);
        }

        $am = new DrupalArchiveManager($dm);
        $am->addArchiver(new Archiver\PharArchiver);

        return $am;
    }

}