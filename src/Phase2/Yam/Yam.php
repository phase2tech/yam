<?php

/*
 * This file is part of Yam.
 *
 * (c) Jordi Boggiano <j.boggiano@seld.be>
 *     Nils Adermann <naderman@naderman.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Phase2\Yam;

/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class Yam
{
    const VERSION = '0.0.1-dev';
}
