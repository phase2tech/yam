Yam - Package Repository Generator for Drupal
====================================

Simple static Composer repository generator.

It uses any composer.json file as input and dumps all the required (according
to their version constraints) packages to a Composer Repository file.

In order to work with Drush Make files, Yam needs to run as the archiver for
included project repositories, see the Satis documentation on how to configure the [archive](http://getcomposer.org/doc/articles/handling-private-packages-with-satis.md#downloads).

Usage
-----

Yam works just like [Satis](https://github.com/composer/satis):

- Download [Composer](https://getcomposer.org/download/): `curl -sS https://getcomposer.org/installer | php`
- Install satis: `php composer.phar create-project bmcmurray/yam --stability=dev --keep-vcs`
- Build a repository: `php bin/yam build <composer.json> <build-dir>`

Read the more detailed instructions from Satis in the 
[Satis documentation](http://getcomposer.org/doc/articles/handling-private-packages-with-satis.md).

Updating
--------

Updating is as simple as running `git pull && php composer.phar install` in the yam directory.

Contributing
------------

All code contributions - including those of people having commit access -
must go through a pull request and approved by a core developer before being
merged. This is to ensure proper review of all the code.

Fork the project, create a feature branch, and send us a pull request.

Requirements
------------

PHP 5.3+

Authors
-------
Yam is written by:

Brian McMurray - <https://github.com/bmcmurray> - [Phase2 Technology](http://phase2technology.com)

Satis is written by:

Jordi Boggiano - <j.boggiano@seld.be> - <http://twitter.com/seldaek> - <http://seld.be><br />
Nils Adermann - <naderman@naderman.de> - <http://twitter.com/naderman> - <http://www.naderman.de><br />

See also the list of [contributors](https://github.com/composer/satis/contributors) who participated in this project.

License
-------

Yam is licensed under the MIT License - see the LICENSE file for details
